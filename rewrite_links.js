//*****************************************************************************
// fixURL( givenURL )
// - "Fix" analytics values in a URL 
//*****************************************************************************
function fixURL( givenUrl ) {
    // split the link
    var link_parts = givenUrl.split( '?' );
    // break up the parameters
    var parameters = link_parts[1].split( '&' );
    // handle each parameter
    for( var j = 0; j < parameters.length; j++ ) {
        // split the parameter for key, value
        var param = parameters[j].split( '=' );
        // if it starts with utm_ then it's googly nonsense
        if( param[0].indexOf( 'utm_' ) > -1 ) {
            // add junk
            param[1] = "FuckOff";
        }
        // reassemble
        parameters[j] = param.join( '=' );
    }
    // reassembling
    link_parts[1] = parameters.join( '&' );
    // last step
    return link_parts.join( '?' );
}

//*****************************************************************************
// updateLinks()
// - Scan and fix the links
//*****************************************************************************
function updateLinks() {
    var links = document.getElementsByTagName('a');

    for( var i = 0; i < links.length; i++ ) {
        // link is to an external domain
        if( links[i].href.indexOf( window.location.hostname ) == -1 ) {
            // link contains compaign non-sense
            if( links[i].href.indexOf( 'utm_' ) > -1 ) {
                links[i].href = fixURL( links[i].href );
            }
        }
    }
}

// Take-off!
updateLinks();
