# WhatCampaign

This browser plug-in replaces utm\_\* parameters in URLs with a fixed string of characters.

Created by @c0debabe, inspired by chats with the ONI team. 
