This "software" is offered as-is with no guarantees.

You can use this "software" as long as you uphold human rights, dignity, and concepts such as:

- Black Lives Matter
- Trans rights are human rights
- Sex work is work
- Everyone should have housing, food, and medical care
- If you can't afford to pay your employees wages they can live on, then you can't afford employees
- Billionaries are unethical &mdash; no one should have that much money
- Remote work is good for bottom-lines and the environment
- The world is round
- Vaccines work
- Companies are neither family nor people
- Advertising is bad for people