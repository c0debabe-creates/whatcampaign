//*****************************************************************************
// fixURL( givenURL )
// - "Fix" analytics values in a URL 
//*****************************************************************************
function fixURL( givenUrl ) {
    // split the link
    var link_parts = givenUrl.split( '?' );
    // break up the parameters
    var parameters = link_parts[1].split( '&' );
    // handle each parameter
    for( var j = 0; j < parameters.length; j++ ) {
        // split the parameter for key, value
        var param = parameters[j].split( '=' );
        // if it starts with utm_ then it's googly nonsense
        if( param[0].indexOf( 'utm_' ) > -1 ) {
            // add junk
            param[1] = "FuckOff";
        }
        // reassemble
        parameters[j] = param.join( '=' );
    }
    // reassembling
    link_parts[1] = parameters.join( '&' );
    // last step
    return link_parts.join( '?' );
}


var loc = window.location.href;
var newloc = loc;
var hasUTM = ( newloc.indexOf( 'utm_' ) > -1 );
var isFixed = ( newloc.indexOf('FuckOff') > -1 );
if( hasUTM && !isFixed ) {
	newloc = fixURL( newloc );
    window.location=newloc;
}

